import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'invite',
    component: () => import('../views/InviteResponse')
  },
  {
    path: '/create',
    name: 'create',
    component: () => import('../views/CreateCampaign')
  },
  {
    path: '/list',
    name: 'list',
    component: () => import('../views/ListCampaigns')
  }
]

const router = new VueRouter({
  routes
})

export default router
