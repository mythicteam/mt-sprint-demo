# Campaign Management Demo

### Description
This is a small app to demo the features and interface developed in the Campaign Management Design Sprint. It's a stand-alone app, meant to simulate the real experience a user would have with our app.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

